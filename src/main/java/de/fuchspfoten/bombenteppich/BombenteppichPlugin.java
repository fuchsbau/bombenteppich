package de.fuchspfoten.bombenteppich;

import de.fuchspfoten.fuchslib.data.PlayerData;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Random;

/**
 * Plugin main class.
 */
public class BombenteppichPlugin extends JavaPlugin implements Listener {

    /**
     * The random number generator.
     */
    private final Random random = new Random();

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        final PlayerData data = new PlayerData(event.getPlayer());
        if (!data.hasFlag("bombenteppich.firstjoin")) {
            data.setFlag("bombenteppich.firstjoin");
            grantEquipment(event.getPlayer());
            getLogger().info("Granted initial equipment to " + event.getPlayer().getName());
        }
    }

    /**
     * Grants equipment to the given player. The granted equipment may vary depending on implementation.
     * @param player The player to grant equipment to.
     */
    private void grantEquipment(final Player player) {
        final Collection<ItemStack> itemsToAdd = new ArrayList<>();

        final ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
        final LeatherArmorMeta helmetMeta = (LeatherArmorMeta) helmet.getItemMeta();
        helmetMeta.setColor(randomColor());
        helmetMeta.setDisplayName("§6Fellmütze mit Bommel");
        helmet.setItemMeta(helmetMeta);
        itemsToAdd.add(helmet);

        final ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
        final LeatherArmorMeta chestplateMeta = (LeatherArmorMeta) chestplate.getItemMeta();
        chestplateMeta.setColor(randomColor());
        chestplateMeta.setDisplayName("§6Kuschelpullover");
        chestplate.setItemMeta(chestplateMeta);
        itemsToAdd.add(chestplate);

        final ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
        final LeatherArmorMeta leggingsMeta = (LeatherArmorMeta) leggings.getItemMeta();
        leggingsMeta.setColor(randomColor());
        leggingsMeta.setDisplayName("§6Pyjamahose");
        leggings.setItemMeta(leggingsMeta);
        itemsToAdd.add(leggings);

        final ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
        final LeatherArmorMeta bootsMeta = (LeatherArmorMeta) boots.getItemMeta();
        bootsMeta.setColor(randomColor());
        bootsMeta.setDisplayName("§6Flauschpantoffeln");
        boots.setItemMeta(bootsMeta);
        itemsToAdd.add(boots);

        itemsToAdd.add(new ItemStack(Material.STONE_SWORD));
        itemsToAdd.add(new ItemStack(Material.STONE_PICKAXE));
        itemsToAdd.add(new ItemStack(Material.STONE_AXE));
        itemsToAdd.add(new ItemStack(Material.PUMPKIN_PIE, 16));
        itemsToAdd.add(new ItemStack(Material.COOKED_BEEF, 16));

        final Map<Integer, ItemStack> remains = player.getInventory().addItem(itemsToAdd.toArray(new ItemStack[0]));
        for (final ItemStack remain : remains.values()) {
            player.getWorld().dropItem(player.getLocation(), remain);
        }

    }

    /**
     * Generates a random color.
     * @return The randomly generated color.
     */
    private Color randomColor() {
        return Color.fromRGB(random.nextInt(256), random.nextInt(256), random.nextInt(256));
    }
}
